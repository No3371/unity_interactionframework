﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BA_Studio.UnityLib.Caster2D
{
	[CustomEditor(typeof(CircleCaster2D))]
	public class CircleCaster2DInspector : PhysicsCaster2DV2Inspector {


		private CircleCaster2D Target;

		GUIContent dirLabel = new GUIContent("Cast Direction"), pointsLabel = new GUIContent("Cast Points");

		public void OnEnable () {
			base.OnEnable();
		}

		public override void OnInspectorGUI()
		{
			EditorGUI.BeginChangeCheck();
			Target = (CircleCaster2D)target;
			GUILayout.Label("Caster Settings", EditorStyles.boldLabel);
			DrawCommonFields(Target);
			GUILayout.Space(8);
			GUILayout.Label("Circle Settings", EditorStyles.boldLabel);
			GUILayout.BeginVertical();
				Target.castRadius = EditorGUILayout.FloatField("Cast Radius", Target.castRadius);
			GUILayout.EndVertical();        
			if (EditorGUI.EndChangeCheck()) SceneView.RepaintAll();

			serializedObject.ApplyModifiedProperties();
		}

		public void OnSceneGUI () {
			if (Target == null) return;
			if (!Target.ToggleVisualization) return;
			if (EditorApplication.isPlaying) offsetPos.Set(Target.transform.position.x + Target.CastBaseOffsetFlipped.x, Target.transform.position.y + Target.CastBaseOffsetFlipped.y);
			else offsetPos.Set(Target.transform.position.x + Target.castBaseOffset.x, Target.transform.position.y + Target.castBaseOffset.y);
			Handles.DrawWireDisc(offsetPos, Vector3.back, Target.castRadius);
		}
	}


}

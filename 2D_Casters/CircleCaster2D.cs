﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BA_Studio.UnityLib.Caster2D
{

	public class CircleCaster2D : PhysicsCaster2DV2
	{

		[HideInInspector]
		public float castRadius = 1.2f, castRadiusCache;

		bool radiusOneShot;

		void Awake ()
		{
		}

		public override void Start ()
		{
			base.Start();
		}

		public override void Update ()
		{
			base.Update();
		}

		void OnDrawGizmos ()
		{
			#if UNITY_EDITOR
				if (UnityEditor.EditorApplication.isPlaying) if (baseTransform != null) if (ToggleVisualization) Gizmos.DrawWireSphere(baseTransform.position + (Vector3) CastBaseOffsetFlipped, castRadius);
				else {
					if (baseTransform != null) if (ToggleVisualization) Gizmos.DrawWireSphere(baseTransform.position + (Vector3) castBaseOffset, castRadius);
				}
			#endif
		}

		protected override void DoCast ()
		{
			PreCast();

			casted = new Collider2D[maxCandidate];

			if (cache == null) cache = new List<Collider2D>();
			else cache.Clear();

			Physics2D.OverlapCircle(baseTransform.position + (Vector3) CastBaseOffsetFlipped, castRadius, filter2D, casted);

			if (casted.All(e => e == null)) return;

			cache.AddRange(casted);

			cache.RemoveAll(e => e== null);
			cache.RemoveAll(e => blackList.Contains(e));
			if (whiteListMode) cache.RemoveAll(e => !targetTags.Contains(e.tag));
			else {
				cache.RemoveAll(e => ignoreTags.Contains(e.tag));	
			}

			DoneCast();
		}

		protected override void DoneCast()
		{
		}

		Collider2D[] casted;
		List<Collider2D> cache;

		public override bool Check<T> ()
		{			
			DoCast();
			if (cache.Count == 0) return false;
			else return true;
		}

		public override T[] CastAll<T> ()
		{
			DoCast();
			cache.RemoveAll(e => e.GetComponent<T>() == null);
			if (cache.Count == 0) return null;
			return cache.SelectMany(e => e.GetComponents<T>()).ToArray();
		}

		public override T Cast<T> ()
		{
			DoCast();
			cache.RemoveAll(e => e.GetComponent<T>() == null);
			if (cache.Count == 0) return null;
			return FindClosest(cache).GetComponent<T>();

		}

		Collider2D FindClosest (List<Collider2D> list) {
			Collider2D got = null;
			float closestDis = 0;
			
			foreach (Collider2D col2 in list) {
				float dis = (baseTransform.position - col2.transform.position).sqrMagnitude;
				if (got == null || closestDis > dis) {
					got = col2;
					closestDis = dis;
				}
			}
			return got;
		}

		public void SetRadius (float Radius) {
			this.castRadius = Radius;
		}

		public void SetRadiusOneShot (float Radius) {
			this.castRadiusCache = castRadius;
			this.castRadius = Radius;
			radiusOneShot = true;
		}
	}
}
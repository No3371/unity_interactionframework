using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using BA_Studio.UnityLib.General;

namespace BA_Studio.UnityLib.InteractionSystem
{
	[System.Serializable]
	public class InteractionArrayEvent : UnityEvent<Interaction[]> {}
	
	[System.Serializable]
	public class InteractionEvent : UnityEvent<Interaction> {}
	[System.Serializable]
	public class PreInteractionArrayEvent : UnityEvent<PreInteraction[]> {}
	
	[System.Serializable]
	public class PreInteractionEvent : UnityEvent<PreInteraction> {}
}

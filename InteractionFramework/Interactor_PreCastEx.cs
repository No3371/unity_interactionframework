using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using BA_Studio.UnityLib.General;

namespace BA_Studio.UnityLib.InteractionSystem
{
	[RequireComponent(typeof(Interactor))]
	public class Interactor_PreCastEx : MonoBehaviour
	{
		public Interactor main;

		public PreInteraction[] preCasted;


		[Header("Update/FixedUpdate")]
		public UpdateFrequency frequency;

		[Range(1, 60)]
		[Tooltip("For every n UpdateFrequency")]
		public int performanceFactor;

		int f = 5;

		public PreInteractionEvent casted;

		public PreInteractionArrayEvent castedMulti;

		void Awake ()
		{
			if (main == null) main = this.GetComponent<Interactor>();
		}

		void Update ()
		{
			if (frequency == UpdateFrequency.Update)
			{
				if (f >= performanceFactor) 
				{
					PreCast();
					f = 0;
				}
				f++;
			}
		}

		void FixedUpdate()
		{
			if (frequency == UpdateFrequency.FixedUpdate)
			{
				if (f >= performanceFactor) 
				{
					PreCast();	
					f = 0;
				}
				f++;
			}
		}

		public void PreCast ()
		{
			preCasted = main.caster.CastAll<PreInteraction>();
			if (preCasted != null)
			{
				if (preCasted.Length == 1)
				{
					preCasted[0].PreCasted(this);
					casted?.Invoke(preCasted[0]);
				}
				else if (preCasted.Length > 1)
				{
					foreach (PreInteraction pi in preCasted) pi.PreCasted(this);
					castedMulti?.Invoke(preCasted);
				}
			}
		}
	}
}

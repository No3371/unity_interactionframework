﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using BA_Studio.UnityLib.Caster2D;

namespace BA_Studio.UnityLib.InteractionSystem
{
	public class Interactor : MonoBehaviour
	{

		public string id;
		public CircleCaster2D caster;

		internal IEnumerable<Interaction> casted;

		Dictionary<System.Type, object> interactionData;

		public Dictionary<System.Type, object> InteractionData
		{
			get
			{
				if (interactionData == null) interactionData = new Dictionary<System.Type, object>();
				return interactionData;
			}
		}

		HashSet<string> blackList;

		HashSet<string> BlackList
		{
			get
			{
				if (blackList == null) blackList = new HashSet<string>();
				return blackList;
			}
		}

		HashSet<string> whiteList;

		HashSet<string> WhiteList
		{
			get
			{
				if (whiteList == null) whiteList = new HashSet<string>();
				return whiteList;
			}
		}

		IEnumerable<T> GetFiltered<T> (IEnumerable<T> source, bool tryAvaliable = true, bool orderByPriority = true) where T : Interaction
		{
			IEnumerable<T> temp;
			if (WhiteList.Count > 0)
			{
				Debug.Log("Whitelisted:");
				foreach (string t in WhiteList) Debug.Log(t);
				temp = source.Where(
							c => WhiteList.Any(s => s == c.GetType().AssemblyQualifiedName)
							&& ((tryAvaliable)? TryAvaliable(c) : true));
				Debug.Log("(" + this.id + ") Interactor whitelist enabled!");
			}
			else if (BlackList.Count > 0)
			{
				Debug.Log("Blacklisted:");
				foreach (string t in BlackList)
				{
					Debug.Log(t);
				}
				temp = source.Where(
							c => (!BlackList.Contains(c.GetType().AssemblyQualifiedName))
							&& ((tryAvaliable)? TryAvaliable(c) : true));
			}
			else temp = source;
			
			if (temp == null || temp.Count() == 0)
			{
				Debug.Log("Filtered out!");
				return null;
			}

			Debug.Log("Result:");
			foreach (T t in temp) Debug.Log(t.ID + "(" + t.GetType().AssemblyQualifiedName + ")" + " is blacklisted? " + BlackList.Contains(t.GetType().AssemblyQualifiedName));
			if (orderByPriority) temp = temp.OrderByDescending(c => c.priority);
			return temp;

		}

		int highestPriorityCache, indexCache;

		void Awake ()
		{
			if (caster == null) caster = PhysicsCaster2DV2.GetCasterByID<CircleCaster2D>(this.gameObject, "Interaction");
		}

		bool TryAvaliable (Interaction i)
		{			
			if (InteractionData.ContainsKey(i.GetType()))
			{				
				return i.Avaliable(this, InteractionData[i.GetType()]);
			}
			else
			{
				return i.Avaliable(this);
			}
		}

		public bool TryInteractNearest (params System.Action[] fallback)
		{
			return TryInteractNearest<Interaction>(fallback);
		}

		public bool TryInteractTopOne (params System.Action[] fallback)
		{
			return TryInteractTopOne<Interaction>(fallback);
		}
		
		public bool TryInteractID (string ID, params System.Action[] fallback)
		{
			return TryInteractID<Interaction>(ID, fallback);
		}

		public bool TryInteractAll (bool forceAll = false, params System.Action[] fallback)
		{
			return TryInteractAll<Interaction>(forceAll, fallback);
		}

		public bool TryInteractNearest<T> (params System.Action[] fallback) where T : Interaction
		{
			casted = null;
			casted = caster.CastAll<T>();
			if (casted == null)
			{
				foreach (System.Action a in fallback) a.Invoke();
				return false;
			}

			T i = GetFiltered(casted, true, true).ElementAt(0) as T;
			if (i != null)
			{
				DoInteract(i);
				return true;
			}
			else
			{
				foreach (System.Action a in fallback) a.Invoke();
				return false;
			}
		}

		public bool TryInteractTopOne<T> (params System.Action[] fallback) where T : Interaction
		{
			casted = null;
			//Debug.Log("(" + this.id + ")Interactor casting for top prioritized " + typeof(T));
			casted = caster.CastAll<T>();
			//Debug.Log("Casted: ");
			if (casted != null) foreach(T t in casted) Debug.Log(t.GetType().AssemblyQualifiedName);
			if (casted == null || casted.Count() == 0 || (casted = GetFiltered(casted, true, true)) == null)
			{
				foreach (System.Action a in fallback) a.Invoke();
				return false;
			}

			DoInteract(casted.ElementAt(0));
			return true;
		}

		public bool TryInteractID<T> (string ID, params System.Action[] fallback) where T : Interaction
		{
			casted = null;
			Debug.Log("(" + this.id + ")Interactor casting for " + typeof(T) + " with ID: " + ID);
			casted = caster.CastAll<T>();
			if (casted == null || casted.Count() == 0 || (casted = GetFiltered(casted, true, true)) == null || !casted.Any(c => c.ID.Equals(ID)))
			{
				foreach (System.Action a in fallback) a.Invoke();
				return false;
			}

			DoInteract(casted.First(c => c.ID.Equals(ID)));
			return true;
		}

		public bool TryInteractAll<T> (bool forceAll = false, params System.Action[] fallback) where T : Interaction
		{
			casted = null;
			Debug.Log("(" + this.id + ")Interactor casting for all possible " + typeof(T));
			casted = caster.CastAll<T>();
			if (casted == null || casted.Count() == 0 || (casted = GetFiltered(casted, true, true)) == null)
			{
				foreach (System.Action a in fallback) a.Invoke();
				return false;
			}

			for (int i = 0; i < casted.Count(); i++)
			{
				DoInteract(casted.ElementAt(i));
				if (i < casted.Count() - 1)
					if (!casted.ElementAt(i).AllowNextInteraction(this, casted.ElementAt(i+1)))
						if (!forceAll) break;
			}
			return true;
		}

		public void DoInteract (Interaction interaction)
		{
			Debug.Log("(" + this.id + ")Interactor trying to interact a " + interaction.GetType());
			if (interactionData.ContainsKey(interaction.GetType())) interaction.Interacted(this, interactionData[interaction.GetType()]);
			else interaction.Interacted(this);
		}

		public void SetData (System.Type type, object data)
		{
			if (InteractionData.ContainsKey(type)) InteractionData[type] = data;
			else InteractionData.Add(type, data);
		}

		public void AddToBlackList<T> () where T : Interaction
		{
			this.BlackList.Add(typeof(T).AssemblyQualifiedName);
		}

		public void RemoveFromBlackList<T> () where T : Interaction
		{
			this.BlackList.Remove(typeof(T).AssemblyQualifiedName);
		}
		
		public void AddToWhiteList<T> () where T : Interaction
		{
			this.WhiteList.Add(typeof(T).AssemblyQualifiedName);
		}

		public void RemoveFromWhiteList<T> () where T : Interaction
		{
			this.WhiteList.Remove(typeof(T).AssemblyQualifiedName);
		}
	}
}
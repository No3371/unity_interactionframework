﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace BA_Studio.UnityLib.InteractionSystem
{
	public abstract class Interaction : MonoBehaviour
	{
		[RangeAttribute(0, 99)]
		public int priority;

		public string ID;

		public abstract void Interacted (Interactor actor, object data = null);
		
		public virtual bool Avaliable (Interactor actor)
		{
			return true;
		}
		public virtual bool Avaliable (Interactor actor, object data)
		{
			throw new RequireDataException(this.GetType() + "'s Avaliable() is executed but not defined.");
		}

		public abstract bool AllowNextInteraction (Interactor actor, Interaction action);

		public static T FindTopPriority<T> (Interaction[] interactions) where T : Interaction {
			return interactions.Where(e => e is T).OrderByDescending((i) => i.priority).First() as T;
		}
		public static Interaction FindTopPriority (Interaction[] interactions) 
		{
			return interactions.OrderByDescending((i) => i.priority).First();
		}

		public static Dictionary<string, Interaction> ActiveInteractionLookup;

		public static void SetInteractionActive (string ID, Interaction interaction)
		{
			ActiveInteractionLookup.Add(ID, interaction);
		}

		public static void SetInteractionInActive (string ID)
		{
			ActiveInteractionLookup.Remove(ID);
		}

		public static T GetActiveInteractionByID<T> (string ID) where T : Interaction
		{
			return (ActiveInteractionLookup[ID] as T) ?? null;
		}

	}

	public abstract class TimedInteraction : ToggleInteraction
	{
		public float duration;
    }

	public abstract class ToggleInteraction : Interaction
	{
		public bool isInteracting;

		public override void Interacted (Interactor actor, object data)
		{
			StartInteraction(actor, data);
		}

		public abstract void StartInteraction (Interactor actor, object data);

		public abstract void EndInteraction (Interactor actor, object data);
	}

	[System.Serializable]
	public class RequireDataException : System.Exception
	{
		public RequireDataException() { }
		public RequireDataException(string message) : base(message) { }
		public RequireDataException(string message, System.Exception inner) : base(message, inner) { }
		protected RequireDataException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
	
	[System.Serializable]
	public class WrongDataException : System.Exception
	{
		public WrongDataException() { }
		public WrongDataException(string message) : base(message) { }
		public WrongDataException(string message, System.Exception inner) : base(message, inner) { }
		protected WrongDataException(
			System.Runtime.Serialization.SerializationInfo info,
			System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
	}
}
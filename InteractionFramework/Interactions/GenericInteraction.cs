﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace BA_Studio.UnityLib.InteractionSystem
{
	public class GenericInteraction : Interaction {

		public UnityEvent beforeInteracted, interacted, afterInteracted;
		
		public override void Interacted (Interactor actor, object data = null)
		{			
			if (beforeInteracted != null) beforeInteracted.Invoke();
			if (interacted != null) interacted.Invoke();
			if (afterInteracted != null) afterInteracted.Invoke();
		}
		
		public override bool AllowNextInteraction (Interactor actor, Interaction action) {
			return false;
		}

		public override bool Avaliable (Interactor actor) {
			return true;
		}

	}
}
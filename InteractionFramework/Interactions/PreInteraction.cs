﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace BA_Studio.UnityLib.InteractionSystem
{
	[RequireComponent(typeof(Interaction))]
	public abstract class PreInteraction : MonoBehaviour {

		public Interaction bind;

		public abstract void PreCasted (Interactor_PreCastEx caster);

		public abstract void PreCasted (Interactor_PreCastEx actor, object data);
		
		public abstract bool Avaliable (Interactor_PreCastEx actor);

		public static T FindTopPriority<T> (PreInteraction[] interactions) where T : PreInteraction
		{
			return interactions.Where(i => i is T).OrderByDescending(i => i.bind.priority).First() as T;
		}

		public static PreInteraction FindTopPriority (PreInteraction[] interactions) 
		{
			return interactions.OrderByDescending((i) => i.bind.priority).First();
		}

	}
}